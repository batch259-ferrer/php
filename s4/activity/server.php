<?php
session_start();

if (isset($_POST['action'])) {
  if ($_POST['action'] === 'login') {
    

    $valid_username = 'johnsmith@gmail.com';
    $valid_password = '1234';

    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($username === $valid_username && $password === $valid_password) {
      $_SESSION['username'] = $username;
      header('Location: index.php');
      exit();
    } else {
      
      echo 'Invalid username or password';
    }
  } elseif ($_POST['action'] === 'logout') {
    session_unset();
    session_destroy();
    header('Location: index.php');
    exit();
  }
}
?>