<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity</title>
</head>
<body>
    <?php session_start(); ?>


    <?php if(isset($_SESSION['username'])): ?>
        <h3>Hello, <?php echo $_SESSION['username']; ?></h3>
        <form method="POST" action="./server.php" style="display: inline-block;">
            <input type="hidden" name="action" value="logout">

            <button type="submit">Logout</button>
        </form>

    <?php else: ?>

        <h3>Login</h3>

        <form method="POST" action="./server.php" style="display: inline-block;">

            <input type="hidden" name="action" value="login">

            Username: <input type="text" name="username" required>

            Password: <input type="password" name="password" required>

            <button type="submit">Login</button>

        </form>

    <?php endif; ?>

</body>
</html>