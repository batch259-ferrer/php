<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s03B: Access Modifiers and Encapsulation</title>

</head>
<body>
    
    <h1>Person</h1>
    <?php echo $person->printName(); ?>
  
    <h1>Developer</h1>

    <?php echo $developer->printName(); ?>

    <h1>Engineer</h1>  
    <?php echo $engineer->printName(); ?>

    

</body>
</html>