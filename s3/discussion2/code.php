<?php


class Building {

//Access Modifiers

    // 1. public: fully open, property and methods can be accessed from anywhere
    /* 2. private: 
    - disables direct access to an object's property or methods
    - private modifier also disable inheritance of its properties and methods to child class
    - method and property can only be accessed within the class
    */
    /*3. protected:
        - this modifier will allow inheritance of properties and methods to child classes
        - property or method is only accessible within the class and child class
    */
   
	protected $name;
	protected $floor;
	protected $address;

	public function __construct($name, $floor, $address ) {

		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

}

class Condominium extends Building {


    //getter amd setter
    //This is use to retrieve and modify the values.

    //getter - is use to retrieve
    public function getName(){
        return $this->name;
    } 
    //setter - mutator
    //this is use to change the default values of an instantiated object
    public function setName($name)
    // $this->name = $name; - pwedeng irekta
    {
       if(gettype($name) === "string"){
        $this->name = $name;
       }
    }

}

$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');