<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Object and Classes</h1>

    <h3>Objects as Variables</h3>

    <p><?php var_dump($buildingObj) ?></p>
    <p><?php echo $buildingObj -> name; ?></p>

    <h3>Object created using Building Class</h3>
    <p><?php var_dump($building) ?></p>
    <p> <?php echo $building->name ?></p>
    <p> <?php echo $building->printName() ?></p>

    <h3>Modifying the instantiated Object</h3>
    <?php $building->name = 'GMA Network'; ?>
    <p><?php var_dump($building) ?></p>
    <p> <?php echo $building->name ?></p>


    <h3>Inheritance (Condominium)</h3>
    <p><?php var_dump($condominium); ?></p>
    <p><?php echo $condominium->name; ?></p>
    <p><?php echo $condominium->floor; ?></p>


    <h3>Polymorphism</h3>
    <p><?php echo $condominium->printName(); ?></p>

</body>
</html>