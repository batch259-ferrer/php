<?php



$buildingObj = (object) [
    'name' => 'Caswynn Building',
    'floor' => 8,
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

$buildingObj2 = (object) [
    'name' => 'GMA Network',
    'floor' => 20,
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];


//Objects from Classes

class Building {
    //Properties (instead of variables)
    public $name;
    public $floor;
    public $address;
     
    //Constructor Function
    //A constructor allows us to initialize an object's properties upon creation of the object 
    public function __construct($name, $floor, $address){


        //Properties (instead of variables)
        //Characteristic
        $this -> name = $name;
        $this -> floor = $floor;
        $this -> address = $address;
    }

    //Methods
    //An action that object can perform

    public function printName(){
        return "The name of the building is $this->name.";
    }

}

//Inheritance and Polymorphism

// Inheritance - The derived classes are allowed to inherit properties and methods from a specified base class.
    //The "extends" keyword is use to derive a class from another class.
    //A derive/child class has all the properties and methods of the parent class.
    //PARENT CLASS => BUILDING
    //CHILD CLASS => CONDOMINIUM

    class Condominium extends Building {
        
        // Polymorphism - Methods inherited by a child/derived class can be overriden to have a behavior different from the method of the base/parent class.

        public function printName() {
         return "The name of the condominium is $this->name.";
        }
}




//instantiate

$building = new Building('Caswynn Building', 8, 'Timog Avenue., Quezon City, Philippines');

//Inheritance and Polymorphism
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');