<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s01: PHP Basics and Selection Control</title>
</head>
<body>
        <!-- <h1>Hello World</h1> -->

        <h1> Echoing Values </h1>

        <p> <?php echo 'Good day $name! Your given emails is $email.'; ?> </p>
        
        <p> <?php echo 'Good day ' . $name . ' ! '  .  'Your given emails is '  .  $email . ' . '; ?> </p>

        <p> <?php "Good day $name! Your given email is $email."; ?>  </p>
        

        <p> <?php echo PI; ?></p>

        <p> <?php echo $headCount; ?></p>



        <h1>Data Types</h1>

        <h2>1. Strings</h2>

        <p> <?php echo $address; ?></p>

        <h2>2. Integers / float </h2>

        <p><?php echo $age; ?></p>
        <p><?php echo $grade; ?></p>

        <h2>3. Boolean / Null</h2>
        <p><?php echo "hasTravelledAbroad: $hasTravelledAbroad"; ?></p>
        <p><?php echo "spouse: $spouse"; ?></p>

        <!-- gettype() or var_dump -->
        <h2>4. Get Type</h2>
        <p><?php echo gettype($hasTravelledAbroad); ?> </p>
        <p><?php echo gettype($spouse); ?></p>

        <!-- var_dump function dumps information about one or more
            variables
        -->
        <h2>5. Var Dump</h2>
        <p><?php var_dump($hasTravelledAbroad); ?> </p>
        <p><?php var_dump($spouse); ?></p>
        <p><?php var_dump($state); ?></p>


        <h2>6. Arrays</h2>
        <p><?php var_dump($grades); ?></p>
        <p><?php print_r($grades); ?></p>
        <p><?php echo $grades[3]; ?></p>


        <h2>7. Object</h2>
        <p><?php var_dump($personObject); ?></p>
        <p><?php print_r($personObject); ?></p>
        <p><?php echo $gradesObject->firstGrading; ?></p>
        <p><?php echo $personObject->address->state; ?></p>

        <h2>8. Operators</h2>
        
        <p>X: <?php echo $x; ?></p>
        <p>Y: <?php echo $y; ?></p>

        <p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
        <p>Is Registered Age: <?php echo var_dump($isRegistered); ?></p>


        <h2>9. Arithmetic Operators</h2>
        <p>Sum: <?php echo $x + $y; ?></p>
        <p>Difference: <?php echo $x - $y; ?></p>
        <p>Product: <?php echo $x * $y; ?></p>
        <p>Quotient: <?php echo $x / $y; ?></p>
        <p>Modulo: <?php echo $x % $y; ?></p>

        <h2>10. Equality Operators</h2>
        <p>Loose Equality: <?php echo var_dump($x == '500') ?></p>
        <p>Strict Equality: <?php echo var_dump($x === '500') ?></p>
        <p>Loose Inequality: <?php echo var_dump($x != '500') ?></p>
        <p>Strict Inequality: <?php echo var_dump($x !== '500') ?></p>

        <h2>11. Greater / Lesser Operators</h2>
        <p>Is Lesser: <?php echo var_dump($x < $y) ?></p>
        <p>Is Greater: <?php echo var_dump($x > $y) ?></p>
        <p>Is Lesser or Equal: <?php echo var_dump($x <= $y) ?></p>
        <p>Is Greater or Equal: <?php echo var_dump($x >= $y) ?></p>

        <h2>12. Logical Operators</h2>
        <p>Are all Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>

        <p>Are some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>

        <p>Are some Requirements Not Met: <?php echo var_dump(!$isLegalAge || !$isRegistered); ?></p>

        <p>Are some Requirements Not Met: <?php echo var_dump(!$isLegalAge || !$isRegistered); ?></p>

    

        <h2>13. Function</h2>
        <p>Fullname: <?php echo getFullName('John', 'D', 'Smith') ?></p>


        <h2>14. Selection Control Structures</h2>
        <h3>If-Else-if-Else</h3>

        <p><?php echo determineTyphoonIntensity(12) ?></p>

        <h2>14. Ternary Sample (Is Underage?)</h2>

        <p>25: <?php var_dump(isUnderAge(25)); ?></p>
        <p>17: <?php var_dump(isUnderAge(17)); ?></p>


        <h2>15. Swtich</h2>
        <p> <?php echo determineComputerUser(4) ?></p>


        <h2>Try-Catch-Finally</h2>
        <p> <?php greeting("Hello"); ?> </p>
        <p> <?php greeting(25); ?> </p>

</body>
</html>


