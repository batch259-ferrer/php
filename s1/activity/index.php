<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity</title>
</head>
<body>

    <h1>Get Full Address</h1>
    
    <p>FullAddress: <?php echo getFullAddress('3F Caswynn Bldg.', 'Timog Avenue,Quezon City', 'Metro Manila', 'Philippines') ?></p>

    <p>FullAddress: <?php echo getFullAddress('3F Enzo Bldg,.', 'Buendia Avenue,Makati City', 'Metro Manila', 'Philippines') ?></p>

    <h1>Create Grade</h1>

    <p>87 is equivalent to <?php echo getLetterGrade(87) ?></p>
    <p>94 is equivalent to <?php echo getLetterGrade(94) ?></p>
    <p>74 is equivalent to <?php echo getLetterGrade(74) ?></p>

</body>
</html> 